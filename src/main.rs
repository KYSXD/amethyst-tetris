mod components;
mod config;
mod systems;
mod tetris;

use amethyst::{
    core::transform::TransformBundle,
    input::InputBundle,
    input::StringBindings,
    prelude::*,
    renderer::plugins::{RenderFlat2D, RenderToWindow},
    renderer::types::DefaultBackend,
    renderer::RenderingBundle,
    utils::application_root_dir,
};

use tetris::Tetris;

use crate::config::GameBoardConfig;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;

    let display_config_path = app_root.join("config").join("display_config.ron");

    let binding_path = app_root.join("config").join("bindings_config.ron");
    let input_bundle =
        InputBundle::<StringBindings>::new().with_bindings_from_file(binding_path)?;

    let gameboard_config_path = app_root.join("config").join("gameboard_config.ron");
    let gameboard_config = GameBoardConfig::load(&gameboard_config_path);

    let game_data = GameDataBuilder::default()
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)
                        .with_clear([0.0, 0.0, 0.0, 1.0]),
                )
                .with_plugin(RenderFlat2D::default()),
        )?
        .with_bundle(TransformBundle::new())?
        .with_bundle(input_bundle)?
        .with(systems::FallingSystem::new(), "falling_system", &[])
        .with(systems::ControllerSystem::new(), "controller_system", &[])
        .with(systems::GameGridSystem::new(), "gamegrid_system", &[]);

    let mut game = Application::build("./", Tetris)?
        .with_resource(gameboard_config)
        .build(game_data)?;

    game.run();
    Ok(())
}
