use amethyst::{
    core::{ParentHierarchy, Transform},
    ecs::prelude::{Entities, Join, ReadExpect, ReadStorage, System, WriteStorage},
    renderer::SpriteRender,
};

use std::collections::HashSet;

use crate::components::{FallingComponent, Monomino, Polymino};
use crate::tetris::{GameGrid, MinoSprite, STEP_FALL_LEN};

pub struct FallingSystem {
    counter: i32,
}

impl FallingSystem {
    pub fn new() -> FallingSystem {
        FallingSystem { counter: 0 }
    }
}

// let col = transform.global_matrix().column(3).xyz().x;

impl<'s> System<'s> for FallingSystem {
    type SystemData = (
        ReadStorage<'s, Polymino>,
        WriteStorage<'s, Transform>,
        WriteStorage<'s, Monomino>,
        ReadStorage<'s, FallingComponent>,
        ReadExpect<'s, ParentHierarchy>,
        WriteStorage<'s, SpriteRender>,
        ReadExpect<'s, GameGrid>,
        ReadExpect<'s, MinoSprite>,
        Entities<'s>,
    );

    fn run(
        &mut self,
        (
            polyminos,
            mut transforms,
            mut monominos,
            falls,
            parenthierarchy,
            mut sprites,
            gamegrid,
            mino_sprite,
            entities,
        ): Self::SystemData,
    ) {
        // game loop
        self.counter += 1;

        if self.counter > STEP_FALL_LEN {
            self.counter = 0;

            let mut colliding_entities = HashSet::new();

            // loop for polyminos...
            for (e, _, _) in (&entities, &polyminos, &falls).join() {
                // ...with transform...
                if transforms.get(e).is_some() {
                    // ... each of its children...
                    let mut colliding = false;

                    // ...to find collisions...
                    for c in parenthierarchy.children(e) {
                        let transform = transforms.get(e).unwrap();
                        let (col, row) = {
                            let child_transform = transforms.get(*c).unwrap();

                            (
                                (transform.translation().x + child_transform.translation().x)
                                    as usize,
                                (transform.translation().y + child_transform.translation().y - 1.0)
                                    as usize,
                            )
                        };

                        colliding |= {
                            (row < gamegrid.cells.len()
                                && col < gamegrid.cells[row].len()
                                && gamegrid.cells[row][col] == true)
                        }
                    }

                    // ...and delete or move...
                    if colliding {
                        colliding_entities.insert(e);
                    } else {
                        transforms.get_mut(e).unwrap().prepend_translation_y(-1.0);
                    }
                }
            }

            // delete colliding
            for e in colliding_entities.iter() {
                // create replacements
                for c in parenthierarchy.children(*e) {
                    let transform = {
                        let mut temp = Transform::default();

                        let x = {
                            transforms.get(*c).unwrap().translation().x
                                + transforms.get(*e).unwrap().translation().x
                        };
                        let y = {
                            transforms.get(*c).unwrap().translation().y
                                + transforms.get(*e).unwrap().translation().y
                        };

                        temp.set_translation_xyz(x, y, 0.0);
                        temp
                    };

                    let sprite_render = SpriteRender {
                        sprite_sheet: mino_sprite.sprite.clone(),
                        sprite_number: 0,
                    };

                    entities
                        .build_entity()
                        .with(sprite_render, &mut sprites)
                        .with(transform, &mut transforms)
                        .with(Monomino::new(), &mut monominos)
                        .build();
                }

                // create new tetramino
                let mut transform = Transform::default();
                transform.set_translation_xyz(4.0, 20.0, 0.0);

                // delete current mino ?
                transforms.get_mut(*e).unwrap().set_translation_x(4.0);
                transforms.get_mut(*e).unwrap().set_translation_y(20.0);
                // entities.delete(*e).expect("No entity exist");
            }
        }
    }
}
