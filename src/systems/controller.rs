use amethyst::{
    core::Transform,
    ecs::prelude::{Join, Read, ReadStorage, System, WriteStorage},
    input::InputHandler,
    input::StringBindings,
};

use crate::components::Owner;

pub struct ControllerSystem;

impl ControllerSystem {
    pub fn new() -> ControllerSystem {
        ControllerSystem {}
    }
}

impl<'s> System<'s> for ControllerSystem {
    type SystemData = (
        ReadStorage<'s, Owner>,
        WriteStorage<'s, Transform>,
        Read<'s, InputHandler<StringBindings>>,
    );

    fn run(&mut self, (owners, mut transforms, input): Self::SystemData) {
        // TODO: Use Owner
        for (transform, owner) in (&mut transforms, &owners).join() {
            let move_left = input
                .action_is_down(&format!("GameMode.{:?}.left", owner.id))
                .unwrap_or(false);
            let move_right = input
                .action_is_down(&format!("GameMode.{:?}.right", owner.id))
                .unwrap_or(false);

            let multiplier = match (move_left, move_right) {
                (true, _) => Some(-1),
                (_, true) => Some(1),
                (_, _) => None,
            };

            if let Some(var) = multiplier {
                transform.prepend_translation_x(var as f32);
            }

            let rotate_clockwise = input
                .action_is_down(&format!("GameMode.{:?}.rotate.clockwise", owner.id))
                .unwrap_or(false);
            let rotate_counter_clockwise = input
                .action_is_down(&format!("GameMode.{:?}.rotate.counter_clockwise", owner.id))
                .unwrap_or(false);

            let multiplier = match (rotate_clockwise, rotate_counter_clockwise) {
                (true, _) => Some(-90_f32.to_radians()),
                (_, true) => Some(90_f32.to_radians()),
                (_, _) => None,
            };

            if let Some(var) = multiplier {
                transform.rotate_2d(var);
            }
        }
    }
}
