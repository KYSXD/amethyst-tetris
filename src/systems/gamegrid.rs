use amethyst::{
    core::{Parent, Transform},
    ecs::prelude::{Join, ReadStorage, System, WriteExpect},
};

use crate::components::{FallingComponent, Monomino};
use crate::tetris::GameGrid;

pub struct GameGridSystem;

impl GameGridSystem {
    pub fn new() -> GameGridSystem {
        GameGridSystem {}
    }
}

impl<'s> System<'s> for GameGridSystem {
    type SystemData = (
        ReadStorage<'s, Transform>,
        ReadStorage<'s, Parent>,
        ReadStorage<'s, FallingComponent>,
        ReadStorage<'s, Monomino>,
        WriteExpect<'s, GameGrid>,
    );

    fn run(&mut self, (transforms, parents, falls, monominos, mut gamegrid): Self::SystemData) {
        // Reset grid
        for row in gamegrid.cells.iter_mut() {
            for cell in row.iter_mut() {
                *cell = false;
            }
        }

        // Loop isolated transforms
        for (_, transform, _, _) in (&monominos, &transforms, !&parents, !&falls).join() {
            let col = transform.translation().x as usize;
            let row = transform.translation().y as usize;

            // Check row is valid
            if row < gamegrid.cells.len() {
                // Check col is valid
                if col < gamegrid.cells[row].len() {
                    gamegrid.cells[row][col] = true;
                }
            }
        }
    }
}
