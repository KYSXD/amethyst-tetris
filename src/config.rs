// use std::path::Path;
// use amethyst::config::Config;

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct GameBoardConfig {
    pub rows: u32,
    pub cols: u32,
}

impl Default for GameBoardConfig {
    fn default() -> Self {
        GameBoardConfig {
            rows: 24u32,
            cols: 10u32,
        }
    }
}
