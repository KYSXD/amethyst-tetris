use amethyst::ecs::prelude::{Component, NullStorage};

#[derive(Default)]
pub struct Polymino;

impl Polymino {
    pub fn new() -> Polymino {
        Polymino {}
    }
}

impl Component for Polymino {
    type Storage = NullStorage<Self>;
}
