use amethyst::ecs::prelude::{Component, DenseVecStorage};

// Debug is required to use as string
// TODO: Remove debug, use structs
#[derive(PartialEq, Eq, Debug)]
pub enum PlayerId {
    Player1,
}

pub struct Owner {
    pub id: PlayerId,
    pub x: i32,
    pub y: i32,
}

impl Component for Owner {
    type Storage = DenseVecStorage<Self>;
}

impl Owner {
    pub fn new(id: PlayerId, x: i32, y: i32) -> Owner {
        Owner { id, x, y }
    }
}
