use amethyst::ecs::prelude::{Component, DenseVecStorage};

pub struct FallingComponent {
    pub foo: f32,
}

impl FallingComponent {
    pub fn new(foo: f32) -> FallingComponent {
        FallingComponent { foo }
    }
}

impl Component for FallingComponent {
    type Storage = DenseVecStorage<Self>;
}
