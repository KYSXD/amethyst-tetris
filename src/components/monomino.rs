use amethyst::ecs::prelude::{Component, NullStorage};

#[derive(Default)]
pub struct Monomino;

impl Monomino {
    pub fn new() -> Monomino {
        Monomino {}
    }
}

impl Component for Monomino {
    type Storage = NullStorage<Self>;
}
