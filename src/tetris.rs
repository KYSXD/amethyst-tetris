use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    core::{Parent, Transform},
    prelude::*,
    renderer::{
        formats::texture::ImageFormat, Camera, SpriteRender, SpriteSheet, SpriteSheetFormat,
        Texture,
    },
};

use crate::components::{FallingComponent, Monomino, Owner, PlayerId, Polymino};
use crate::config::GameBoardConfig;

pub struct GameGrid {
    pub cells: Vec<Vec<bool>>,
}

pub struct MinoSprite {
    pub sprite: Handle<SpriteSheet>,
}

pub const STEP_FALL_LEN: i32 = 60;

pub struct Tetris;

impl SimpleState for Tetris {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        let sprite_sheet_handle = load_sprite_sheet(world);

        initialize_monomino(world, sprite_sheet_handle.clone());
        initialize_tetraminos(world, sprite_sheet_handle.clone());
        initialize_player(world, sprite_sheet_handle.clone());
        initialize_camera(world);
        initialize_gamegrid(world);
    }
}

fn initialize_gamegrid(world: &mut World) {
    let (rows, cols) = {
        let config = &world.read_resource::<GameBoardConfig>();
        (config.rows as f32, config.cols as f32)
    };

    world.insert(GameGrid {
        cells: vec![vec![false; cols as usize]; rows as usize],
    });
}

fn initialize_camera(world: &mut World) {
    let mut transform = Transform::default();

    let (rows, cols) = {
        let config = &world.read_resource::<GameBoardConfig>();
        (config.rows as f32, config.cols as f32)
    };

    // Setup camera in a way that our screen covers whole arena
    transform.set_translation_xyz(cols / 2.0 - 0.5, rows / 2.0 - 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(cols as f32, rows as f32))
        .with(transform)
        .build();
}

fn initialize_tetraminos(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    for (x_, y_, color) in [
        // T shape
        (0, 0, 0),
        (1, 0, 0),
        (2, 0, 0),
        (1, 1, 0),
        // L shape
        (7, 0, 1),
        (8, 0, 1),
        (8, 1, 1),
        (8, 2, 1),
        // Square Shape
        (2, 1, 3),
        (2, 2, 3),
        (3, 1, 3),
        (3, 2, 3),
        // I shape
        (0, 1, 4),
        (0, 2, 4),
        (0, 3, 4),
        (0, 4, 4),
        // S Shape
        (3, 0, 6),
        (4, 0, 6),
        (4, 1, 6),
        (5, 1, 6),
    ]
    .iter()
    {
        let sprite_render = SpriteRender {
            sprite_sheet: sprite_sheet.clone(),
            sprite_number: *color,
        };

        let mut transform = Transform::default();
        transform.set_translation_xyz(*x_ as f32, *y_ as f32, 0.0);

        world
            .create_entity()
            .with(sprite_render.clone())
            .with(transform)
            .with(Monomino::new())
            .build();
    }
}

fn initialize_monomino(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    for (x_, y_, color) in [(9, 20, 2)].iter() {
        let sprite_render = SpriteRender {
            sprite_sheet: sprite_sheet.clone(),
            sprite_number: *color,
        };

        let mut transform = Transform::default();
        transform.set_translation_xyz(*x_ as f32, *y_ as f32, 0.0);

        world
            .create_entity()
            .with(sprite_render.clone())
            .with(transform)
            .with(FallingComponent::new(1.0))
            .with(Monomino::new())
            .build();
    }
}

fn initialize_player(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    // Parent
    let (xi, yi) = (4, 20);

    let mut transform = Transform::default();
    transform.set_translation_xyz(xi as f32, yi as f32, 0.0);

    let e = world
        .create_entity()
        .with(transform)
        .with(FallingComponent::new(1.0))
        .with(Owner::new(PlayerId::Player1, 22, 5))
        .with(Polymino::new())
        .build();

    // Childs (monominos)
    for (x_, y_, color) in [
        // Z Shape
        (1, 0, 5),
        (0, 0, 5),
        (0, 1, 5),
        (-1, 1, 5),
    ]
    .iter()
    {
        let sprite_render = SpriteRender {
            sprite_sheet: sprite_sheet.clone(),
            sprite_number: *color,
        };

        let mut transform = Transform::default();
        transform.set_translation_xyz(*x_ as f32, *y_ as f32, 0.0);

        world
            .create_entity()
            .with(sprite_render.clone())
            .with(transform)
            .with(Parent::new(e))
            .with(Monomino::new())
            .build();
    }
}

fn load_sprite_sheet(world: &mut World) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/block_spritesheet.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let sprite_handle = {
        let loader = world.read_resource::<Loader>();
        let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();

        loader.load(
            "texture/block_spritesheet.ron",
            SpriteSheetFormat(texture_handle),
            (),
            &sprite_sheet_store,
        )
    };

    world.insert(MinoSprite {
        sprite: sprite_handle.clone(),
    });
    sprite_handle
}
