Amethyst Tetris
===============

An attempt to make a tetris clone with the [Amethyst Game Engine](https://amethyst.rs/).

How to use
----------

```bash
cargo run --features=vulkan
```

![](images/test-20191227.png)
