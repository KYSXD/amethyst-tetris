build_linux:
	cargo build --features vulkan

run_linux:
	cargo run --features vulkan

.PHONY: build_linux run_linux
